package com.fredericdinand.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.fredericdinand.tp1.data.models.Result;

public class ResultActivity extends AppCompatActivity {

    private TextView activityNameTextView;
    private TextView turnOverTextView;
    private TextView contributionTextView;
    private TextView profitTextView;
    private Result result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = this.getIntent();
        Bundle bundle = intent.getExtras();
        result = (Result) bundle.getSerializable("RESULT");
        setContentView(R.layout.activity_result);
        activityNameTextView = findViewById(R.id.professional_text_view);
        turnOverTextView = findViewById(R.id.turn_over_text_view);
        contributionTextView = findViewById(R.id.contribution_text_view);
        profitTextView = findViewById(R.id.profit_text_view);
    }

    @Override
    protected void onResume() {
        super.onResume();
        activityNameTextView.setText(getString(R.string.professional_activity, result.getProfessionalActivity().getName()));
        turnOverTextView.setText(getString(R.string.turn_over, result.getTurnover()));
        double contribution = (result.getTurnover() * result.getProfessionalActivity().getContributionPercent());
        contributionTextView.setText(getString(R.string.professional_contribution, result.getTurnover(), result.getProfessionalActivity().getContribution(), contribution));
        profitTextView.setText(getString(R.string.professional_profits, result.getTurnover(), contribution, result.getTurnover() - contribution));
    }
}