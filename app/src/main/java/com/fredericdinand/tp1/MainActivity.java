package com.fredericdinand.tp1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.fredericdinand.tp1.data.models.ProfessionalActivity;
import com.fredericdinand.tp1.data.models.Result;
import com.fredericdinand.tp1.data.services.data.DataService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {
    private DataService dataService = DataService.getInstance();

    private Spinner dropdown;
    private EditText editText;
    private Button validateButton;
    private Result result;
    private ArrayList<ProfessionalActivity> professionalActivities = new ArrayList<>();
    private ProfessionalActivity professionalActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        professionalActivities = dataService.getProfessionalActivities(getApplicationContext());

        dropdown = findViewById(R.id.dropdown);

        List<String> names = professionalActivities.stream().map(x->x.getName()).collect(Collectors.toList());
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, names);
        dropdown.setAdapter(adapter);
        dropdown.setOnItemSelectedListener(this);

        editText = findViewById(R.id.edit_text);

        validateButton = findViewById(R.id.validate_button);
        validateButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        if (id == R.id.validate_button) {
            if (editText.getText().length() == 0 || professionalActivity == null) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.all_fields_required), Toast.LENGTH_SHORT).show();
                return;
            }

            int turnover = Integer.parseInt(editText.getText().toString());
            result = new Result(professionalActivity, turnover);

            if (turnover == 0) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.invalid_turnover), Toast.LENGTH_SHORT).show();
                return;
            }

            Intent intent = new Intent(MainActivity.this, ResultActivity.class);
            Bundle bundle = new Bundle();
            bundle.putSerializable("RESULT", this.result);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        professionalActivity = this.professionalActivities.get(position);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        professionalActivity = null;
    }
}