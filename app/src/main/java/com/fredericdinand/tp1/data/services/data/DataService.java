package com.fredericdinand.tp1.data.services.data;

import android.content.Context;

import com.fredericdinand.tp1.R;
import com.fredericdinand.tp1.data.models.ProfessionalActivity;

import java.util.ArrayList;

public class DataService {
    private DataService() {}

    private static DataService INSTANCE = new DataService();

    public static DataService getInstance() {
        return INSTANCE;
    }

    public ArrayList<ProfessionalActivity> getProfessionalActivities(Context context) {
        ArrayList<ProfessionalActivity> array = new ArrayList<>();

        array.add(new ProfessionalActivity(context.getResources().getString(R.string.merchandise_sale), 12.8));
        array.add(new ProfessionalActivity(context.getResources().getString(R.string.rent), 22));
        array.add(new ProfessionalActivity(context.getResources().getString(R.string.tourism_rent), 6));
        array.add(new ProfessionalActivity(context.getResources().getString(R.string.service_provision), 22));
        array.add(new ProfessionalActivity(context.getResources().getString(R.string.liberal_profession), 22));

        return array;
    }
}
