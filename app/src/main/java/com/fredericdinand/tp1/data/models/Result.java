package com.fredericdinand.tp1.data.models;

import java.io.Serializable;

public class Result implements Serializable {
    private static final long serialVersionUID = 1L;
    protected ProfessionalActivity professionalActivity;
    protected int turnover;

    public Result(ProfessionalActivity professionalActivity, int turnover) {
        this.professionalActivity = professionalActivity;
        this.turnover = turnover;
    }

    public ProfessionalActivity getProfessionalActivity() {
        return this.professionalActivity;
    }

    public int getTurnover() {
        return this.turnover;
    }
}
