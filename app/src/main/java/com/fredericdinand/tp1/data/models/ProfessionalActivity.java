package com.fredericdinand.tp1.data.models;

import java.io.Serializable;

public class ProfessionalActivity implements Serializable {
    private static final long serialVersionUID = 1L;
    protected String name;
    protected double contribution;

    public ProfessionalActivity(String name, double contribution) {
        this.name = name;
        this.contribution = contribution;
    }

    public String getName() {
        return this.name;
    }

    public double getContribution() {
        return this.contribution;
    }

    public double getContributionPercent(){
        return contribution / 100;
    }
}
